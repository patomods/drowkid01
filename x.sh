#!/bin/bash

x='"'
for xz in `echo "dropbear ssl sockspy v2ray slowdns xray udps ss-ssr badvpn openvpn"`; do
echo -e "function banner_$xz(){\nclear;msg -bar;echo -e $x" >> banners.sh
toilet -f future "$xz" >> banners.sh
echo -e "$x\nmsg -bar\n}" >> banners.sh
done

for xz in `echo "dropbear ssl sockspy v2ray slowdns xray udps ss-ssr badvpn openvpn"`; do
echo -e " --$xz) banner_$xz;;" >> banners.sh
done
