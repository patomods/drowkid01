#!/bin/sh
clear&&clear
fun_ip () {
MEU_IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
MEU_IP2=$(wget -qO- ipv4.icanhazip.com)
[[ "$MEU_IP" != "$MEU_IP2" ]] && IP="$MEU_IP2" || IP="$MEU_IP"
trojanport=`lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN" | grep trojan | awk '{print substr($9,3); }' > /tmp/trojan.txt && echo | cat /tmp/trojan.txt | tr '\n' ' ' > /bin/ejecutar/trojanports.txt && cat /bin/ejecutar/trojanports.txt`;
troport=$(cat /bin/ejecutar/trojanports.txt  | sed 's/\s\+/,/g' | cut -d , -f1)
portFTP=$(lsof -V -i tcp -P -n | grep apache2 | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN" | cut -d: -f2 | cut -d' ' -f1 | uniq)
portFTP=$(echo ${portFTP} | sed 's/\s\+/,/g' | cut -d , -f1)
}
#FUN_BAR
fun_bar () { 
comando="$1"  
_=$( $comando > /dev/null 2>&1 ) & > /dev/null 
pid=$! 
while [[ -d /proc/$pid ]]; do 
echo -ne " \033[1;33m["    
for((i=0; i<20; i++)); do    
echo -ne "\033[1;31m##"    
sleep 0.5    
done 
echo -ne "\033[1;33m]" 
sleep 1s 
echo 
tput cuu1
 tput dl1 
done 
echo -e " \033[1;33m[\033[1;31m########################################\033[1;33m] - \033[1;32m100%\033[0m" 
sleep 1s 
} 

install_ini () {
add-apt-repository universe
apt update -y; apt upgrade -y
clear
msg -bar3
echo -e "\033[92m        -- INSTALANDO PAQUETES NECESARIOS -- "
msg -bar3
#bc
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] || apt-get install golang-go -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "golang-go"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install golang-go............ $ESTATUS "
#jq
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || apt-get install jq -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install jq................... $ESTATUS "
#curl
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || apt-get install curl -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install curl................. $ESTATUS "
#npm
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || apt-get install npm -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install npm.................. $ESTATUS "
#nodejs
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || apt-get install nodejs -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install nodejs............... $ESTATUS "
#socat
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || apt-get install socat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install socat................ $ESTATUS "
#netcat
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || apt-get install netcat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat............... $ESTATUS "
#net-tools
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || apt-get net-tools -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install net-tools............ $ESTATUS "
#figlet
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || apt-get install figlet -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install figlet............... $ESTATUS "
msg -bar3
echo -e "\033[92m La instalacion de paquetes necesarios a finalizado"
msg -bar3
echo -e "\033[97m Si la instalacion de paquetes tiene fallas"
echo -ne "\033[97m Puede intentar de nuevo [s/n]: "
read inst
[[ $inst = @(s|S|y|Y) ]] && install_ini
echo -ne "\033[97m Deseas agregar Menu Clash Rapido [s/n]: "
read insta
[[ $insta = @(s|S|y|Y) ]] && enttrada
}


fun_insta(){
fun_ip
install_ini
msg -bar3
killall clash 1> /dev/null 2> /dev/null
echo -e " ➣ Creando Directorios y Archivos"
msg -bar3 
[[ -d /root/.config ]] && rm -rf /root/.config/* || mkdir /root/.config 
mkdir /root/.config/clash 1> /dev/null 2> /dev/null
last_version=$(curl -Ls "https://api.github.com/repos/emirjorge/clash-core/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
arch=$(arch)
if [[ $arch == "x86_64" || $arch == "x64" || $arch == "amd64" ]]; then
  arch="amd64"
elif [[ $arch == "aarch64" || $arch == "arm64" ]]; then
  arch="arm64"
else
  arch="amd64"
fi
wget -N --no-check-certificate -O /root/.config/clash/clash.gz https://github.com/emirjorge/clash-core/releases/download/${last_version}/clash-linux-${arch}-${last_version}.gz
gzip -d /root/.config/clash/clash.gz
chmod +x /root/.config/clash/clash
echo -e " ➣ Clonando Repositorio Original Dreamacro "
go get -u -v github.com/Dreamacro/clash
clear
}



[[ -e /bin/ejecutar/msg ]] && source /bin/ejecutar/msg || source <(curl -sSL https://gitlab.com/patomods/drowkid01/-/raw/main/exec/msg-bar/msg)
numero='^[0-9]+$'
hora=$(printf '%(%H:%M:%S)T') 
fecha=$(printf '%(%D)T')
[[ ! -d /bin/ejecutar/clashFiles ]] && mkdir /bin/ejecutar/clashFiles
clashFiles='/bin/ejecutar/clashFiles/'

INITClash(){
msg -bar
conFIN
read -p "Ingrese Nombre del Poster WEB de la configuracion: " cocolon
[[ -e /root/.config/clash/config.yaml ]] && sed -i "s%_dAtE%${fecha}%g" /root/.config/clash/config.yaml
[[ -e /root/.config/clash/config.yaml ]] && sed -i "s/_h0rA/${hora}/g" /root/.config/clash/config.yaml
cp /root/.config/clash/config.yaml /var/www/html/$cocolon.yaml && chmod +x /var/www/html/$cocolon.yaml
service apache2 restart
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33mClash Server Instalado"
echo -e "-------------------------------------------------------"
echo -e "		\033[4;31mNOTA importante\033[0m"
echo -e "Recuerda Descargar el Fichero, o cargarlo como URL!!"
echo -e "-------------------------------------------------------"
echo -e " \033[0;31mSi Usas Clash For Android, Ultima Version  "
echo -e "  Para luego usar el Link del Fichero, y puedas ."
echo -e " Descargarlo desde cualquier sitio con acceso WEB"
echo -e "        Link Clash Valido por 30 minutos "
echo -e "    Link : \033[1;42m  http://$IP:${portFTP}/$cocolon.yaml\033[0m"
echo -e "-------------------------------------------------------"
#read -p "PRESIONA ENTER PARA CARGAR ONLINE"
echo -e "\033[1;32mRuta de Configuracion: /root/.config/clash/config.yaml"
echo -e "\033[1;31mPRESIONE ENTER PARA CONTINUAR\033[0m"
scr=$(echo $(($RANDOM*3))|head -c 3)
unset yesno
echo -e " ENLACE VALIDO POR 30 MINUTOS? " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
killall clash > /dev/null &1>&2
screen -dmS clashse_$cocolon /root/.config/clash/clash
echo '#!/bin/bash -e' > /root/.config/clash/$cocolon.sh
echo "sleep 1800s" >> /root/.config/clash/$cocolon.sh && echo -e " ACTIVO POR 30 MINUTOS "  || echo " Validacion Incorrecta "
echo "mv /var/www/html/$cocolon.yaml ${clashFiles}$cocolon.yaml" >> /root/.config/clash/$cocolon.sh
echo 'echo "Fichero removido a ${clashFiles}$cocolon.yaml"' >> /root/.config/clash/$cocolon.sh
echo "service apache2 restart" >> /root/.config/clash/$cocolon.sh
echo "rm -f /root/.config/clash/$cocolon.sh" >> /root/.config/clash/$cocolon.sh
echo 'exit' >> /root/.config/clash/$cocolon.sh && screen -dmS clash${scr} bash /root/.config/clash/$cocolon.sh
} 
echo -e "Proceso Finalizado"

}

configINIT_rule () {
mode=$1
[[ -z ${mode} ]] && exit
unset tropass
echo '#SCRIPT OFICIAL ChuKK-SCRIPT|Plus
# Formato Creado por @ChuKK-SCRIPT | +593987072611 Whatsapp Personal
# Creado el _dAtE - _h0rA
port: 8080
socks-port: 7891
redir-port: 7892
allow-lan: true
bind-address: "*"
mode: rule
log-level: info
external-controller: "0.0.0.0:9090"
secret: ""

dns:
  enable: true
  listen: :53
  enhanced-mode: fake-ip
  nameserver:
    - 114.114.114.114
    - 223.5.5.5
    - 8.8.8.8
    - 45.71.185.100
    - 204.199.156.138
    - 1.1.1.1
  fallback: []
  fake-ip-filter:
    - +.stun.*.*
    - +.stun.*.*.*
    - +.stun.*.*.*.*
    - +.stun.*.*.*.*.*
    - "*.n.n.srv.nintendo.net"
    - +.stun.playstation.net
    - xbox.*.*.microsoft.com
    - "*.*.xboxlive.com"
    - "*.msftncsi.com"
    - "*.msftconnecttest.com"
    - WORKGROUP    
tun:
  enable: true
  stack: gvisor
  auto-route: true
  auto-detect-interface: true
  dns-hijack:
    - any:53

# Clash for Windows
cfw-bypass:
  - qq.com
  - music.163.com
  - "*.music.126.net"
  - localhost
  - 127.*
  - 10.*
  - 172.16.*
  - 172.17.*
  - 172.18.*
  - 172.19.*
  - 172.20.*
  - 172.21.*
  - 172.22.*
  - 172.23.*
  - 172.24.*
  - 172.25.*
  - 172.26.*
  - 172.27.*
  - 172.28.*
  - 172.29.*
  - 172.30.*
  - 172.31.*
  - 192.168.*
  - <local>
cfw-latency-timeout: 5000
    
proxy-groups:
- name: "ChuKK-SCRIPT-ADM"
  type: select
  proxies:    ' > /root/.config/clash/config.yaml
#sed -i "s/+/'/g"  /root/.config/clash/config.yaml
foc=1
[[ -e /usr/local/etc/trojan/config.json ]] && ConfTrojINI
unset yesno
foc=1
[[ -e /etc/v2ray/config.json ]] && ConfV2RINI
unset yesno
foc=1								
[[ -e /etc/xray/config.json ]] && ConfXRINI
}

configINIT_global () {
mode=$1
[[ -z ${mode} ]] && exit
unset tropass
echo '#SCRIPT OFICIAL ChuKK-SCRIPT|Plus
port: 8080
socks-port: 7891
redir-port: 7892
allow-lan: true
bind-address: "*"
mode: global
log-level: info
external-controller: "0.0.0.0:9090"
secret: ""
dns:
  enable: true
  listen: :53
  enhanced-mode: fake-ip
  nameserver:
    - 114.114.114.114
    - 223.5.5.5
    - 8.8.8.8
    - 45.71.185.100
    - 204.199.156.138
    - 1.1.1.1
  fallback: []
  fake-ip-filter:
    - +.stun.*.*
    - +.stun.*.*.*
    - +.stun.*.*.*.*
    - +.stun.*.*.*.*.*
    - "*.n.n.srv.nintendo.net"
    - +.stun.playstation.net
    - xbox.*.*.microsoft.com
    - "*.*.xboxlive.com"
    - "*.msftncsi.com"
    - "*.msftconnecttest.com"
    - WORKGROUP    
tun:
  enable: true
  stack: gvisor
  auto-route: true
  auto-detect-interface: true
  dns-hijack:
    - any:53

# Clash for Windows
cfw-bypass:
  - qq.com
  - music.163.com
  - "*.music.126.net"
  - localhost
  - 127.*
  - 10.*
  - 172.16.*
  - 172.17.*
  - 172.18.*
  - 172.19.*
  - 172.20.*
  - 172.21.*
  - 172.22.*
  - 172.23.*
  - 172.24.*
  - 172.25.*
  - 172.26.*
  - 172.27.*
  - 172.28.*
  - 172.29.*
  - 172.30.*
  - 172.31.*
  - 192.168.*
  - <local>
cfw-latency-timeout: 5000   
 ' > /root/.config/clash/config.yaml
#sed -i "s/+/'/g"  /root/.config/clash/config.yaml
foc=1
[[ -e /usr/local/etc/trojan/config.json ]] && ConfTrojINI
unset yesno
foc=1
[[ -e /etc/v2ray/config.json ]] && ConfV2RINI
unset yesno
foc=1								
[[ -e /etc/xray/config.json ]] && ConfXRINI
}

proxyTRO() {
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proTRO+="- name: $1\n  type: trojan\n  server: ${IP}\n  port: ${troport}\n  password: "$2"\n  udp: true\n  sni: $3\n  alpn:\n  - h2\n  - http/1.1\n  skip-cert-verify: true\n\n" 
  }

ConfTrojINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG TROJAN " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p " [S/N]: " yesno

tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL TROJAN CLASH "
read -p ": " nameperfil
msg -bar3
[[ -z ${UUID} ]] && view_usert || { 
echo -e " USER ${Usr} : ${UUID}"
msg -bar3
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosni
msg -bar3
proxyTRO ${nameperfil} ${UUID} ${trosni}
ConfTrojINI
								}
}

proxyV2R() {
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proV2R+="- name: $1\n  type: vmess\n  server: ${IP}\n  port: $7\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  udp: true\n  tls: true\n  skip-cert-verify: true\n  servername: $2\n  network: $5\n  ws-opts:  \n       path: $6\n       headers:\n         Host: $2\n  \n\n" 
  }
  
proxyV2Rgprc() {
#config=/usr/local/x-ui/bin/config.json
#cat $config | jq .inbounds[].settings.clients | grep id
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proV2R+="
- name: $1\n  server: ${IP}\n  port: $7\n  type: vmess\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  tls: true\n  skip-cert-verify: true\n  network: grpc\n  servername: $2\n  grpc-opts:\n    grpc-mode: gun\n    grpc-service-name: $6\n  udp: true \n\n"
  }
proxyXR() {
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proXR+="- name: $1\n  type: vmess\n  server: ${IP}\n  port: $7\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  udp: true\n  tls: true\n  skip-cert-verify: true\n  servername: $2\n  network: $5\n  ws-opts:  \n       path: $6\n       headers:\n         Host: $2\n  \n\n" 
  }
  
proxyXRgprc() {
#config=/usr/local/x-ui/bin/config.json
#cat $config | jq .inbounds[].settings.clients | grep id
#proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
fun_ip
[[ $mode = 1 ]] && echo -e "    - $1" >> /root/.config/clash/config.yaml
proXR+="
- name: $1\n  server: ${IP}\n  port: $7\n  type: vmess\n  uuid: $3\n  alterId: $4\n  cipher: auto\n  tls: true\n  skip-cert-verify: true\n  network: grpc\n  servername: $2\n  grpc-opts:\n    grpc-mode: gun\n    grpc-service-name: $6\n  udp: true \n\n"
  }

ConfV2RINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG V2RAY " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL V2RAY CLASH "
read -p ": " nameperfil
msg -bar3
[[ -z ${uid} ]] && view_user || { 
echo -e " USER ${ps}"
msg -bar3
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosni
msg -bar3

		ps=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uid=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiid=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		net=$(jq '.inbounds[].streamSettings.network' $config)
		parche=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $path = null ]] && parche='' 
		v2port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)

[[ $net = '"grpc"' ]] && {
proxyV2Rgprc ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${path} ${v2port}
} || {
proxyV2R ${nameperfil} ${trosni} ${uid} ${aluuiid} ${net} ${parche} ${v2port}
}

ConfV2RINI
	} 
}

ConfXRINI() {
echo -e " DESEAS AÑADIR TU ${foc} CONFIG XRAY " 
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
[[ ${yesno} = @(s|S|y|Y) ]] &&  { 
unset yesno
foc=$(($foc + 1))
echo -ne "\033[1;33m ➣ PERFIL XRAY CLASH "
read -p ": " nameperfilX
msg -bar3
[[ -z ${uidX} ]] && _view_userXR || { 
echo -e " USER ${ps} XRAY"
msg -bar3
}
echo -ne "\033[1;33m ➣ SNI o HOST "
read -p ": " trosniX
msg -bar3
		psX=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uidX=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiidX=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		addX=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && addX=$(wget -qO- ipv4.icanhazip.com)
		hostX=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		netX=$(jq '.inbounds[].streamSettings.network' $config)
		parcheX=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $pathX = null ]] && parcheX='' 
		v2portX=$(jq '.inbounds[].port' $config)
		tlsX=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $netX = '"grpc"' ]] && pathX=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || pathX=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)

[[ $netX = '"grpc"' ]] && {
proxyXRgprc ${nameperfilX} ${trosniX} ${uidX} ${aluuiidX} ${netX} ${pathX} ${v2portX}
} || {
proxyXR ${nameperfilX} ${trosniX} ${uidX} ${aluuiidX} ${netX} ${parcheX} ${v2portX}
}

ConfXRINI
							}
}

confRULE() {
[[ $mode = 1 ]] && echo -e '
  url: http://www.gstatic.com/generate_204
  interval: 300
 
###################################
# ChuKK-SCRIPT-ADM

# By ChuKK-SCRIPT By CGH
- name: "【 ✵ 𝚂𝚎𝚛𝚟𝚎𝚛-𝙿𝚁𝙴𝙼𝙸𝚄𝙼 ✵ 】"
  type: select
  proxies: 
    - "ChuKK-SCRIPT-ADM"

#- name: "【 📱 +593987072611 】"
#  type: select
#  proxies:
#    - "ChuKK-SCRIPT-ADM"

Rule:
# Unbreak
# > Google
- DOMAIN-SUFFIX,googletraveladservices.com,ChuKK-SCRIPT-ADM
- DOMAIN,dl.google.com,ChuKK-SCRIPT-ADM
- DOMAIN,mtalk.google.com,ChuKK-SCRIPT-ADM

# Internet Service Providers ChuKK-SCRIPT-ADM 运营商劫持
- DOMAIN-SUFFIX,17gouwuba.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,186078.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,189zj.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,285680.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,3721zh.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,4336wang.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,51chumoping.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,51mld.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,51mypc.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,58mingri.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,58mingtian.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,5vl58stm.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,6d63d3.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,7gg.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,91veg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,9s6q.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,adsame.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aiclk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,akuai.top,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,atplay.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,baiwanchuangyi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,beerto.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,beilamusi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,benshiw.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bianxianmao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bryonypie.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cishantao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cszlks.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cudaojia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dafapromo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,daitdai.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dsaeerf.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dugesheying.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dv8c1t.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,echatu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,erdoscs.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fan-yong.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,feih.com.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fjlqqc.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fkku194.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,freedrive.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gclick.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,goufanli100.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,goupaoerdai.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gouwubang.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gzxnlk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,haoshengtoys.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hyunke.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ichaosheng.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ishop789.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jdkic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jiubuhua.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jsncke.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,junkucm.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jwg365.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kawo77.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kualianyingxiao.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kumihua.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ltheanine.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,maipinshangmao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,minisplat.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mkitgfs.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mlnbike.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mobjump.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nbkbgd.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,newapi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pinzhitmall.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,poppyta.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qianchuanghr.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qichexin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qinchugudao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,quanliyouxi.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qutaobi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ry51w.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sg536.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifubo.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuce.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuda.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifufu.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuge.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifugu.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuhe.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuhu.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuji.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sifuka.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,smgru.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,taoggou.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tcxshop.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tjqonline.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,topitme.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tt3sm4.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tuia.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tuipenguin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tuitiger.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,websd8.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wsgblw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wx16999.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xchmai.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xiaohuau.xyz,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ygyzx.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yinmong.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yitaopt.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yjqiqi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yukhj.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhaozecheng.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhenxinet.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zlne800.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zunmi.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zzd6.com,ChuKK-SCRIPT-ADM
- IP-CIDR,39.107.15.115/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,47.89.59.182/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,103.49.209.27/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,123.56.152.96/32,ChuKK-SCRIPT-ADM,no-resolve
# > ChinaTelecom
- IP-CIDR,61.160.200.223/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,61.160.200.242/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,61.160.200.252/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,61.174.50.214/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,111.175.220.163/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,111.175.220.164/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,122.229.8.47/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,122.229.29.89/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,124.232.160.178/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,175.6.223.15/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,183.59.53.237/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,218.93.127.37/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,221.228.17.152/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,221.231.6.79/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,222.186.61.91/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,222.186.61.95/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,222.186.61.96/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,222.186.61.97/32,ChuKK-SCRIPT-ADM,no-resolve
# > ChinaUnicom
- IP-CIDR,106.75.231.48/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,119.4.249.166/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,220.196.52.141/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,221.6.4.148/32,ChuKK-SCRIPT-ADM,no-resolve
# > ChinaMobile
- IP-CIDR,114.247.28.96/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,221.179.131.72/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,221.179.140.145/32,ChuKK-SCRIPT-ADM,no-resolve
# > Dr.Peng
# - IP-CIDR,10.72.25.0/24,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,115.182.16.79/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,118.144.88.126/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,118.144.88.215/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,118.144.88.216/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,120.76.189.132/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,124.14.21.147/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,124.14.21.151/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,180.166.52.24/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,211.161.101.106/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,220.115.251.25/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,222.73.156.235/32,ChuKK-SCRIPT-ADM,no-resolve

# Malware 恶意网站
# > 快压
# https://zhuanlan.zhihu.com/p/39534279
- DOMAIN-SUFFIX,kuaizip.com,ChuKK-SCRIPT-ADM
# > MacKeeper
# https://www.lizhi.io/blog/40002904
- DOMAIN-SUFFIX,mackeeper.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zryydi.com,ChuKK-SCRIPT-ADM
# > Adobe Flash China Special Edition
# https://www.zhihu.com/question/281163698/answer/441388130
- DOMAIN-SUFFIX,flash.cn,ChuKK-SCRIPT-ADM
- DOMAIN,geo2.adobe.com,ChuKK-SCRIPT-ADM
# > C&J Marketing 思杰马克丁软件
# https://www.zhihu.com/question/46746200
- DOMAIN-SUFFIX,4009997658.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abbyychina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bartender.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,betterzip.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,betterzipcn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,beyondcompare.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bingdianhuanyuan.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chemdraw.com.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cjmakeding.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cjmkt.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,codesoftchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,coreldrawchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,crossoverchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dongmansoft.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,earmasterchina.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,easyrecoverychina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ediuschina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,flstudiochina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,formysql.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,guitarpro.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,huishenghuiying.com.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hypersnap.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iconworkshop.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,imindmap.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jihehuaban.com.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,keyshot.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kingdeecn.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,logoshejishi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,luping.net.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mairuan.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mairuan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mairuan.com.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mairuan.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mairuanwang.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,makeding.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mathtype.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mindmanager.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mindmanager.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mindmapper.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mycleanmymac.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nicelabel.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ntfsformac.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ntfsformac.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,overturechina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,passwordrecovery.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pdfexpert.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,photozoomchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,shankejingling.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ultraiso.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vegaschina.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xmindchina.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xshellcn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yihuifu.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yuanchengxiezuo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zbrushcn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhzzx.com,ChuKK-SCRIPT-ADM

# Global Area Network
# (ChuKK-SCRIPT-ADM)
# (Music)
# > Deezer
# USER-AGENT,Deezer*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,deezer.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dzcdn.net,ChuKK-SCRIPT-ADM
# > KKBOX
- DOMAIN-SUFFIX,kkbox.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kkbox.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kfs.io,ChuKK-SCRIPT-ADM
# > JOOX
# USER-AGENT,WeMusic*,ChuKK-SCRIPT-ADM
# USER-AGENT,JOOX*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,joox.com,ChuKK-SCRIPT-ADM
# > Pandora
# USER-AGENT,Pandora*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pandora.com,ChuKK-SCRIPT-ADM
# > SoundCloud
# USER-AGENT,SoundCloud*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,p-cdn.us,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sndcdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,soundcloud.com,ChuKK-SCRIPT-ADM
# > Spotify
# USER-AGENT,Spotify*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pscdn.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,scdn.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,spotify.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,spoti.fi,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,spotify.com,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,-spotify-com,ChuKK-SCRIPT-ADM
# > TIDAL
# USER-AGENT,TIDAL*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tidal.com,ChuKK-SCRIPT-ADM
# > YouTubeMusic
# USER-AGENT,com.google.ios.youtubemusic*,ChuKK-SCRIPT-ADM
# USER-AGENT,YouTubeMusic*,ChuKK-SCRIPT-ADM
# (Video)
# > All4
# USER-AGENT,All4*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,c4assets.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,channel4.com,ChuKK-SCRIPT-ADM
# > AbemaTV
# USER-AGENT,AbemaTV*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abema.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ameba.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abema.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hayabusa.io,ChuKK-SCRIPT-ADM
- DOMAIN,abematv.akamaized.net,ChuKK-SCRIPT-ADM
- DOMAIN,ds-linear-abematv.akamaized.net,ChuKK-SCRIPT-ADM
- DOMAIN,ds-vod-abematv.akamaized.net,ChuKK-SCRIPT-ADM
- DOMAIN,linear-abematv.akamaized.net,ChuKK-SCRIPT-ADM
# > Amazon Prime Video
# USER-AGENT,InstantVideo.US*,ChuKK-SCRIPT-ADM
# USER-AGENT,Prime%20Video*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aiv-cdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aiv-delivery.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,amazonvideo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,primevideo.com,ChuKK-SCRIPT-ADM
- DOMAIN,avodmp4s3ww-a.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,d25xi40x97liuc.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,dmqdd6hw24ucf.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,d22qjgkvxw22r6.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,d1v5ir2lpwr8os.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,avoddashs,ChuKK-SCRIPT-ADM
# > Bahamut
# USER-AGENT,Anime*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bahamut.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gamer.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN,gamer-cds.cdn.hinet.net,ChuKK-SCRIPT-ADM
- DOMAIN,gamer2-cds.cdn.hinet.net,ChuKK-SCRIPT-ADM
# > BBC iPlayer
# USER-AGENT,BBCiPlayer*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bbc.co.uk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bbci.co.uk,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,bbcfmt,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,uk-live,ChuKK-SCRIPT-ADM
# > DAZN
# USER-AGENT,DAZN*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dazn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dazn-api.com,ChuKK-SCRIPT-ADM
- DOMAIN,d151l6v8er5bdm.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,voddazn,ChuKK-SCRIPT-ADM
# > Disney+
# USER-AGENT,Disney+*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bamgrid.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,disney-plus.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,disneyplus.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dssott.com,ChuKK-SCRIPT-ADM
- DOMAIN,cdn.registerdisney.go.com,ChuKK-SCRIPT-ADM
# > encoreTVB
# USER-AGENT,encoreTVB*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,encoretvb.com,ChuKK-SCRIPT-ADM
- DOMAIN,edge.api.brightcove.com,ChuKK-SCRIPT-ADM
- DOMAIN,bcbolt446c5271-a.akamaihd.net,ChuKK-SCRIPT-ADM
# > FOX NOW
# USER-AGENT,FOX%20NOW*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fox.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,foxdcg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,theplatform.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uplynk.com,ChuKK-SCRIPT-ADM
# > HBO NOW
# USER-AGENT,HBO%20NOW*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbogo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbonow.com,ChuKK-SCRIPT-ADM
# > HBO GO HKG
# USER-AGENT,HBO%20GO%20PROD%20HKG*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbogoasia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbogoasia.hk,ChuKK-SCRIPT-ADM
- DOMAIN,bcbolthboa-a.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,players.brightcove.net,ChuKK-SCRIPT-ADM
- DOMAIN,s3-ap-southeast-1.amazonaws.com,ChuKK-SCRIPT-ADM
- DOMAIN,dai3fd1oh325y.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,44wilhpljf.execute-api.ap-southeast-1.amazonaws.com,ChuKK-SCRIPT-ADM
- DOMAIN,hboasia1-i.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,hboasia2-i.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,hboasia3-i.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,hboasia4-i.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,hboasia5-i.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN,cf-images.ap-southeast-1.prod.boltdns.net,ChuKK-SCRIPT-ADM
# > 华文电视
# USER-AGENT,HWTVMobile*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,5itv.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ocnttv.com,ChuKK-SCRIPT-ADM
# > Hulu
- DOMAIN-SUFFIX,hulu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,huluim.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hulustream.com,ChuKK-SCRIPT-ADM
# > Hulu(フールー)
- DOMAIN-SUFFIX,happyon.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hulu.jp,ChuKK-SCRIPT-ADM
# > ITV
# USER-AGENT,ITV_Player*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,itv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,itvstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN,itvpnpmobile-a.akamaihd.net,ChuKK-SCRIPT-ADM
# > KKTV
# USER-AGENT,KKTV*,ChuKK-SCRIPT-ADM
# USER-AGENT,com.kktv.ios.kktv*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kktv.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kktv.me,ChuKK-SCRIPT-ADM
- DOMAIN,kktv-theater.kk.stream,ChuKK-SCRIPT-ADM
# > Line TV
# USER-AGENT,LINE%20TV*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,linetv.tw,ChuKK-SCRIPT-ADM
- DOMAIN,d3c7rimkq79yfu.cloudfront.net,ChuKK-SCRIPT-ADM
# > LiTV
- DOMAIN-SUFFIX,litv.tv,ChuKK-SCRIPT-ADM
- DOMAIN,litvfreemobile-hichannel.cdn.hinet.net,ChuKK-SCRIPT-ADM
# > My5
# USER-AGENT,My5*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,channel5.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,my5.tv,ChuKK-SCRIPT-ADM
- DOMAIN,d349g9zuie06uo.cloudfront.net,ChuKK-SCRIPT-ADM
# > myTV SUPER
# USER-AGENT,mytv*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mytvsuper.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tvb.com,ChuKK-SCRIPT-ADM
# > Netflix
# USER-AGENT,Argo*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflix.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflix.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nflxext.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nflximg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nflximg.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nflxso.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nflxvideo.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest0.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest1.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest2.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest3.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest4.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest5.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest6.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest7.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest8.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netflixdnstest9.com,ChuKK-SCRIPT-ADM
- IP-CIDR,23.246.0.0/18,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,37.77.184.0/21,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,45.57.0.0/17,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,64.120.128.0/17,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,66.197.128.0/17,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,108.175.32.0/20,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,192.173.64.0/18,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,198.38.96.0/19,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,198.45.48.0/20,ChuKK-SCRIPT-ADM,no-resolve
# > niconico
# USER-AGENT,Niconico*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dmc.nico,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nicovideo.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nimg.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,socdm.com,ChuKK-SCRIPT-ADM
# > PBS
# USER-AGENT,PBS*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pbs.org,ChuKK-SCRIPT-ADM
# > Pornhub
- DOMAIN-SUFFIX,phncdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pornhub.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pornhubpremium.com,ChuKK-SCRIPT-ADM
# > 台湾好
# USER-AGENT,TaiwanGood*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,skyking.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN,hamifans.emome.net,ChuKK-SCRIPT-ADM
# > Twitch
- DOMAIN-SUFFIX,twitch.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,twitchcdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ttvnw.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jtvnw.net,ChuKK-SCRIPT-ADM
# > ViuTV
# USER-AGENT,Viu*,ChuKK-SCRIPT-ADM
# USER-AGENT,ViuTV*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,viu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,viu.tv,ChuKK-SCRIPT-ADM
- DOMAIN,api.viu.now.com,ChuKK-SCRIPT-ADM
- DOMAIN,d1k2us671qcoau.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,d2anahhhmp1ffz.cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN,dfp6rglgjqszk.cloudfront.net,ChuKK-SCRIPT-ADM
# > YouTube
# USER-AGENT,com.google.ios.youtube*,ChuKK-SCRIPT-ADM
# USER-AGENT,YouTube*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,googlevideo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,youtube.com,ChuKK-SCRIPT-ADM
- DOMAIN,youtubei.googleapis.com,ChuKK-SCRIPT-ADM

# (ChuKK-SCRIPT-ADM)
# > 愛奇藝台灣站
- DOMAIN,cache.video.iqiyi.com,ChuKK-SCRIPT-ADM
# > bilibili
- DOMAIN-SUFFIX,bilibili.com,ChuKK-SCRIPT-ADM
- DOMAIN,upos-hz-mirrorakam.akamaized.net,ChuKK-SCRIPT-ADM

# (DNS Cache Pollution Protection)
# > Google
- DOMAIN-SUFFIX,ampproject.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,appspot.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blogger.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,getoutline.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gvt0.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gvt1.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gvt3.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xn--ngstr-lra8j.com,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,google,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,blogspot,ChuKK-SCRIPT-ADM
# > Microsoft
- DOMAIN-SUFFIX,onedrive.live.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xboxlive.com,ChuKK-SCRIPT-ADM
# > Facebook
- DOMAIN-SUFFIX,cdninstagram.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fb.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fb.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fbaddins.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fbcdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fbsbx.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fbworkmail.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,instagram.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,m.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,messenger.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oculus.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oculuscdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,rocksdb.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whatsapp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whatsapp.net,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,facebook,ChuKK-SCRIPT-ADM
- IP-CIDR,3.123.36.126/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,35.157.215.84/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,35.157.217.255/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,52.58.209.134/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,54.93.124.31/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,54.162.243.80/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,54.173.34.141/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,54.235.23.242/32,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,169.45.248.118/32,ChuKK-SCRIPT-ADM,no-resolve
# > Twitter
- DOMAIN-SUFFIX,pscp.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,periscope.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,t.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,twimg.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,twimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,twitpic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vine.co,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,twitter,ChuKK-SCRIPT-ADM
# > Telegram
- DOMAIN-SUFFIX,t.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tdesktop.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,telegra.ph,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,telegram.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,telegram.org,ChuKK-SCRIPT-ADM
- IP-CIDR,91.108.4.0/22,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,91.108.8.0/22,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,91.108.12.0/22,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,91.108.16.0/22,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,91.108.56.0/22,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,149.154.160.0/20,ChuKK-SCRIPT-ADM,no-resolve
# > Line
- DOMAIN-SUFFIX,line.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,line-apps.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,line-scdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,naver.jp,ChuKK-SCRIPT-ADM
- IP-CIDR,103.2.30.0/23,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,125.209.208.0/20,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,147.92.128.0/17,ChuKK-SCRIPT-ADM,no-resolve
- IP-CIDR,203.104.144.0/21,ChuKK-SCRIPT-ADM,no-resolve
# > Other
- DOMAIN-SUFFIX,4shared.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,520cc.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,881903.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,9cache.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,9gag.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abc.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abc.net.au,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abebooks.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,amazon.co.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apigee.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apk-dl.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apkfind.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apkmirror.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apkmonk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apkpure.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aptoide.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,archive.is,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,archive.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,arte.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,artstation.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,arukas.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ask.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,avg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,avgle.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,badoo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bandwagonhost.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bbc.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,behance.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bibox.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,biggo.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,binance.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bitcointalk.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bitfinex.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bitmex.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bit-z.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bloglovin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bloomberg.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bloomberg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blubrry.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,book.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,booklive.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,books.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,boslife.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,box.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,businessinsider.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bwh1.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,castbox.fm,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cbc.ca,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cdw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,change.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,channelnewsasia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ck101.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,clarionproject.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,clyp.it,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cna.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,comparitech.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,conoha.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,crucial.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cts.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cw.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cyberctm.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dailymotion.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dailyview.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,daum.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,daumcdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dcard.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,deepdiscount.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,depositphotos.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,deviantart.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,disconnect.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,discordapp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,discordapp.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,disqus.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dlercloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dns2go.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dowjones.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dropbox.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dropboxusercontent.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,duckduckgo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dynu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,earthcam.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ebookservice.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,economist.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,edgecastcdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,edu,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,elpais.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,enanyang.my,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,encyclopedia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,esoir.be,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,etherscan.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,euronews.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,evozi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,feedly.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,firech.at,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,flickr.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,flitto.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,foreignpolicy.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,freebrowser.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,freewechat.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,freeweibo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,friday.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ftchinese.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ftimg.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gate.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,getlantern.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,getsync.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,globalvoices.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,goo.ne.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,goodreads.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gov,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gov.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,greatfire.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gumroad.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hbg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,heroku.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hightail.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hk01.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hkbf.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hkbookcity.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hkej.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hket.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hkgolden.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hootsuite.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hudson.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hyread.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ibtimes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,i-cable.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icij.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icoco.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,imgur.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,initiummall.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,insecam.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ipfs.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,issuu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,istockphoto.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,japantimes.co.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jiji.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jinx.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jkforum.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,joinmastodon.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,justmysocks.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,justpaste.it,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kakao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kakaocorp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kik.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kobo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kobobooks.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kodingen.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,lemonde.fr,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,lepoint.fr,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,lihkg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,listennotes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,livestream.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,logmein.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mail.ru,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mailchimp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,marc.info,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,matters.news,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,maying.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,medium.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mega.nz,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mil,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mingpao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mobile01.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myspace.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myspacecdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nanyang.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,naver.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,neowin.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,newstapa.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nexitally.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nhk.or.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nicovideo.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nii.ac.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nikkei.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nofile.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,now.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nrk.no,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nyt.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytcn.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytco.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytimes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytlog.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nytstyle.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ok.ru,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,okex.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,on.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,orientaldaily.com.my,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,overcast.fm,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,paltalk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pao-pao.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,parsevideo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pbxes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pcdvd.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pchome.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,picacomic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pinimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pixiv.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,player.fm,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,plurk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,po18.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,potato.im,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,potatso.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,prism-break.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,proxifier.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pt.im,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pts.org.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pubu.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pubu.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pureapk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,quora.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,quoracdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,rakuten.co.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,readingtimes.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,readmoo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,redbubble.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,reddit.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,redditmedia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,resilio.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,reuters.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,reutersmedia.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,rfi.fr,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,rixcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,roadshow.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,scmp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,scribd.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,seatguru.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,shadowsocks.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,shopee.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,slideshare.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,softfamous.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,soundcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ssrcloud.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,startpage.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamcommunity.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steemit.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steemitwallet.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,t66y.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tapatalk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,teco-hk.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,teco-mo.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,teddysun.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,textnow.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,theguardian.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,theinitium.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,thetvdb.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tineye.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,torproject.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tumblr.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,turbobit.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tutanota.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tvboxnow.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,udn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,unseen.is,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,upmedia.mg,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uptodown.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,urbandictionary.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ustream.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uwants.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,v2ray.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,viber.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,videopress.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vimeo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,voachinese.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,voanews.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,voxer.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vzw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,w3schools.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,washingtonpost.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wattpad.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whoer.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wikimapia.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wikipedia.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wikiquote.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wikiwand.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,winudf.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wire.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wordpress.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,workflow.is,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,worldcat.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wsj.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wsj.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xhamster.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xn--90wwvt03e.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xn--i2ru8q2qg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xnxx.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xvideos.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yahoo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yandex.ru,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ycombinator.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yesasia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yes-news.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yomiuri.co.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,you-get.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zaobao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zb.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zello.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zeronet.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zoom.us,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,github,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,jav,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,pinterest,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,porn,ChuKK-SCRIPT-ADM
- DOMAIN-KEYWORD,wikileaks,ChuKK-SCRIPT-ADM

# (Region-Restricted Access Denied)
- DOMAIN-SUFFIX,apartmentratings.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apartments.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bankmobilevibe.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bing.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,booktopia.com.au,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cccat.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,centauro.com.br,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,clearsurance.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,costco.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,crackle.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,depositphotos.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dish.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dmm.co.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dmm.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dnvod.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,esurance.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,extmatrix.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fastpic.ru,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,flipboard.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fnac.be,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fnac.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,funkyimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fxnetworks.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gettyimages.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,go.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,here.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jcpenney.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jiehua.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mailfence.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nationwide.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nbc.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nexon.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nordstrom.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nordstromimage.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nordstromrack.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,superpages.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,target.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,thinkgeek.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tracfone.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,unity3d.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uploader.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vevo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,viu.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vsco.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xfinity.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zattoo.com,ChuKK-SCRIPT-ADM
# USER-AGENT,Roam*,ChuKK-SCRIPT-ADM

# (The Most Popular Sites)
# > ChuKK-SCRIPT-ADM
# >> TestFlight
- DOMAIN,testflight.apple.com,ChuKK-SCRIPT-ADM
# >> ChuKK-SCRIPT-ADM URL Shortener
- DOMAIN-SUFFIX,appsto.re,ChuKK-SCRIPT-ADM
# >> iBooks Store download
- DOMAIN,books.itunes.apple.com,ChuKK-SCRIPT-ADM
# >> iTunes Store Moveis Trailers
- DOMAIN,hls.itunes.apple.com,ChuKK-SCRIPT-ADM
# >> App Store Preview
- DOMAIN,apps.apple.com,ChuKK-SCRIPT-ADM
- DOMAIN,itunes.apple.com,ChuKK-SCRIPT-ADM
# >> Spotlight
- DOMAIN,api-glb-sea.smoot.apple.com,ChuKK-SCRIPT-ADM
# >> Dictionary
- DOMAIN,lookup-api.apple.com,ChuKK-SCRIPT-ADM
# > Google
- DOMAIN-SUFFIX,abc.xyz,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,android.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,androidify.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dialogflow.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,autodraw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,capitalg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,certificate-transparency.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chrome.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chromeexperiments.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chromestatus.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chromium.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,creativelab5.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,debug.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,deepmind.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,firebaseio.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,getmdl.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ggpht.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gmail.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gmodules.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,godoc.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,golang.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gwtproject.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,itasoftware.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,madewithcode.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,material.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,polymer-project.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,admin.recaptcha.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,recaptcha.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,shattered.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,synergyse.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tensorflow.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tfhub.dev,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tiltbrush.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,waveprotocol.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,waymo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,webmproject.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,webrtc.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whatbrowser.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,widevine.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,x.company,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,youtu.be,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yt.be,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ytimg.com,ChuKK-SCRIPT-ADM
# > Microsoft
# >> Microsoft OneDrive
- DOMAIN-SUFFIX,1drv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,1drv.ms,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blob.core.windows.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,livefilestore.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,onedrive.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,storage.live.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,storage.msn.com,ChuKK-SCRIPT-ADM
- DOMAIN,oneclient.sfx.ms,ChuKK-SCRIPT-ADM
# > Other
- DOMAIN-SUFFIX,0rz.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,4bluestones.biz,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,9bis.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,allconnected.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aol.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bcc.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bit.ly,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bitshare.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blog.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blogimg.jp,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,blogtd.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,broadcast.co.nz,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,camfrog.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cfos.de,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,citypopulation.de,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cloudfront.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ctitv.com.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cuhk.edu.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cusu.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,discord.gg,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,discuss.com.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dropboxapi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,duolingo.cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,edditstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,flickriver.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,focustaiwan.tw,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,free.fr,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gigacircle.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hk-pub.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hosting.co.uk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hwcdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ifixit.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iphone4hongkong.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iphonetaiwan.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iptvbin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,linksalpha.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,manyvids.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myactimes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,newsblur.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,now.im,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nowe.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,redditlist.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,s3.amazonaws.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,signal.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,smartmailcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sparknotes.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,streetvoice.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,supertop.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,typepad.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,udnbkk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,urbanairship.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whispersystems.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wikia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wolframalpha.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,x-art.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yimg.com,ChuKK-SCRIPT-ADM
- DOMAIN,api.steampowered.com,ChuKK-SCRIPT-ADM
- DOMAIN,store.steampowered.com,ChuKK-SCRIPT-ADM

# China Area Network
# > 360
- DOMAIN-SUFFIX,qhres.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qhimg.com,ChuKK-SCRIPT-ADM
# > Akamai
- DOMAIN-SUFFIX,akadns.net,ChuKK-SCRIPT-ADM
# - DOMAIN-SUFFIX,akamai.net,ChuKK-SCRIPT-ADM
# - DOMAIN-SUFFIX,akamaiedge.net,ChuKK-SCRIPT-ADM
# - DOMAIN-SUFFIX,akamaihd.net,ChuKK-SCRIPT-ADM
# - DOMAIN-SUFFIX,akamaistream.net,ChuKK-SCRIPT-ADM
# - DOMAIN-SUFFIX,akamaized.net,ChuKK-SCRIPT-ADM
# > Alibaba
# USER-AGENT,%E4%BC%98%E9%85%B7*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,alibaba.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,alicdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,alikunlun.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,alipay.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,amap.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,autonavi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dingtalk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mxhichina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,soku.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,taobao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tmall.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tmall.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ykimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,youku.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xiami.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xiami.net,ChuKK-SCRIPT-ADM
# > Baidu
- DOMAIN-SUFFIX,baidu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,baidubcr.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bdstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yunjiasu-cdn.net,ChuKK-SCRIPT-ADM
# > bilibili
- DOMAIN-SUFFIX,acgvideo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,biliapi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,biliapi.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bilibili.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bilibili.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hdslb.com,ChuKK-SCRIPT-ADM
# > Blizzard
- DOMAIN-SUFFIX,blizzard.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,battle.net,ChuKK-SCRIPT-ADM
- DOMAIN,blzddist1-a.akamaihd.net,ChuKK-SCRIPT-ADM
# > ByteDance
- DOMAIN-SUFFIX,feiliao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pstatp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,snssdk.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iesdouyin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,toutiao.com,ChuKK-SCRIPT-ADM
# > CCTV
- DOMAIN-SUFFIX,cctv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cctvpic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,livechina.com,ChuKK-SCRIPT-ADM
# > DiDi
- DOMAIN-SUFFIX,didialift.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,didiglobal.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,udache.com,ChuKK-SCRIPT-ADM
# > 蛋蛋赞
- DOMAIN-SUFFIX,343480.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,baduziyuan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,com-hs-hkdy.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,czybjz.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dandanzan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fjhps.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kuyunbo.club,ChuKK-SCRIPT-ADM
# > ChinaNet
- DOMAIN-SUFFIX,21cn.com,ChuKK-SCRIPT-ADM
# > HunanTV
- DOMAIN-SUFFIX,hitv.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mgtv.com,ChuKK-SCRIPT-ADM
# > iQiyi
- DOMAIN-SUFFIX,iqiyi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iqiyipic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,71.am.com,ChuKK-SCRIPT-ADM
# > JD
- DOMAIN-SUFFIX,jd.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jd.hk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jdpay.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,360buyimg.com,ChuKK-SCRIPT-ADM
# > Kingsoft
- DOMAIN-SUFFIX,iciba.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ksosoft.com,ChuKK-SCRIPT-ADM
# > Meitu
- DOMAIN-SUFFIX,meitu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meitudata.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meitustat.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meipai.com,ChuKK-SCRIPT-ADM
# > MI
- DOMAIN-SUFFIX,duokan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mi-img.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,miui.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,miwifi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xiaomi.com,ChuKK-SCRIPT-ADM
# > Microsoft
- DOMAIN-SUFFIX,microsoft.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,msecnd.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,office365.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,outlook.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,s-microsoft.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,visualstudio.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,windows.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,windowsupdate.com,ChuKK-SCRIPT-ADM
- DOMAIN,officecdn-microsoft-com.akamaized.net,ChuKK-SCRIPT-ADM
# > NetEase
# USER-AGENT,NeteaseMusic*,ChuKK-SCRIPT-ADM
# USER-AGENT,%E7%BD%91%E6%98%93%E4%BA%91%E9%9F%B3%E4%B9%90*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,163.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,126.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,127.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,163yun.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,lofter.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netease.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ydstatic.com,ChuKK-SCRIPT-ADM
# > Sina
- DOMAIN-SUFFIX,sina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,weibo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,weibocdn.com,ChuKK-SCRIPT-ADM
# > Sohu
- DOMAIN-SUFFIX,sohu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sohucs.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sohu-inc.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,v-56.com,ChuKK-SCRIPT-ADM
# > Sogo
- DOMAIN-SUFFIX,sogo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sogou.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sogoucdn.com,ChuKK-SCRIPT-ADM
# > Steam
- DOMAIN-SUFFIX,steampowered.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steam-chat.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamgames.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamusercontent.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamcontent.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamcdn-a.akamaihd.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,steamstat.us,ChuKK-SCRIPT-ADM
# > Tencent
# USER-AGENT,MicroMessenger%20Client,ChuKK-SCRIPT-ADM
# USER-AGENT,WeChat*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gtimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,idqqimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,igamecj.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myapp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myqcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qq.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tencent.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tencent-cloud.net,ChuKK-SCRIPT-ADM
# > YYeTs
# USER-AGENT,YYeTs*,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jstucdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zimuzu.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zimuzu.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zmz2019.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zmzapi.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zmzapi.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zmzfile.com,ChuKK-SCRIPT-ADM
# > Content Delivery Network
- DOMAIN-SUFFIX,ccgslb.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ccgslb.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chinanetcenter.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meixincdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ourdvs.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,staticdn.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wangsu.com,ChuKK-SCRIPT-ADM
# > IP Query
- DOMAIN-SUFFIX,ipip.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ip.la,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ip-cdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ipv6-test.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,test-ipv6.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,whatismyip.com,ChuKK-SCRIPT-ADM
# > Speed Test
# - DOMAIN-SUFFIX,speedtest.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,netspeedtestmaster.com,ChuKK-SCRIPT-ADM
- DOMAIN,speedtest.macpaw.com,ChuKK-SCRIPT-ADM
# > Private Tracker
- DOMAIN-SUFFIX,awesome-hd.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,broadcasthe.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chdbits.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,classix-unlimited.co.uk,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,empornium.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gazellegames.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hdchina.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hdsky.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icetorrent.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jpopsuki.eu,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,keepfrds.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,madsrevolution.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,m-team.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nanyangpt.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ncore.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,open.cd,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ourbits.club,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,passthepopcorn.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,privatehd.to,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,redacted.ch,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,springsunday.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tjupt.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,totheglory.im,ChuKK-SCRIPT-ADM
# > Scholar
- DOMAIN-SUFFIX,acm.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,acs.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aip.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ams.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,annualreviews.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aps.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ascelibrary.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,asm.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,asme.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,astm.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bmj.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cambridge.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cas.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,clarivate.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ebscohost.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,emerald.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,engineeringvillage.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icevirtuallibrary.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ieee.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,imf.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,iop.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jamanetwork.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jhu.edu,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jstor.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,karger.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,libguides.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,madsrevolution.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mpg.de,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myilibrary.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nature.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oecd-ilibrary.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,osapublishing.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oup.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ovid.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oxfordartonline.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oxfordbibliographies.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oxfordmusiconline.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,pnas.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,proquest.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,rsc.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sagepub.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sciencedirect.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sciencemag.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,scopus.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,siam.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,spiedigitallibrary.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,springer.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,springerlink.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tandfonline.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,un.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uni-bielefeld.de,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,webofknowledge.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,westlaw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,wiley.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,worldbank.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,worldscientific.com,ChuKK-SCRIPT-ADM
# > Plex Media Server
- DOMAIN-SUFFIX,plex.tv,ChuKK-SCRIPT-ADM
# > Other
- DOMAIN-SUFFIX,cn,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,360in.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,51ym.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,8686c.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,abchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,accuweather.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aicoinstorge.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,air-matters.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,air-matters.io,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aixifan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,amd.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,b612.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bdatu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,beitaichufang.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bjango.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,booking.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,bstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cailianpress.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,camera360.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chinaso.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chua.pro,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chuimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chunyu.mobi,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,chushou.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cmbchina.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cmbimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ctrip.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dfcfw.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,docschina.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,douban.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,doubanio.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,douyu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dxycdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,dytt8.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,eastmoney.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,eudic.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,feng.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,fengkongcloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,frdic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,futu5.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,futunn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gandi.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,geilicdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,getpricetag.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,gifshow.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,godic.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hicloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hongxiu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,hostbuf.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,huxiucdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,huya.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,infinitynewtab.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ithome.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,java.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,jidian.im,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kaiyanapp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kaspersky-labs.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,keepcdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,kkmh.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,licdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,linkedin.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,loli.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,luojilab.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,maoyan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,maoyun.tv,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meituan.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,meituan.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mobike.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,moke.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mubu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,myzaker.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nim-lang-cn.org,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,nvidia.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,oracle.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,paypal.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,paypalobjects.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qdaily.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qidian.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qyer.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,qyerstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,raychase.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ronghub.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ruguoapp.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,s-reader.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sankuai.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,scomper.me,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,seafile.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sm.ms,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,smzdm.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,snapdrop.net,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,snwx.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,sspai.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,takungpao.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,teamviewer.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,tianyancha.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,udacity.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,uning.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,vmware.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,weather.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,weico.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,weidian.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xiachufang.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,ximalaya.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xinhuanet.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,xmcdn.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,yangkeduo.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhangzishi.cc,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhihu.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,zhuihd.com,ChuKK-SCRIPT-ADM
- DOMAIN,download.jetbrains.com,ChuKK-SCRIPT-ADM
- DOMAIN,images-cn.ssl-images-amazon.com,ChuKK-SCRIPT-ADM

# > ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,aaplimg.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apple.co,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apple.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,apple-cloudkit.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,appstore.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,cdn-apple.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,crashlytics.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icloud.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,icloud-content.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,me.com,ChuKK-SCRIPT-ADM
- DOMAIN-SUFFIX,mzstatic.com,ChuKK-SCRIPT-ADM
- DOMAIN,www-cdn.icloud.com.akadns.net,ChuKK-SCRIPT-ADM
- DOMAIN,clash.razord.top,ChuKK-SCRIPT-ADM
- DOMAIN,v2ex.com,ChuKK-SCRIPT-ADM
- IP-CIDR,17.0.0.0/8,ChuKK-SCRIPT-ADM,no-resolve

# Local Area Network
- IP-CIDR,192.168.0.0/16,ChuKK-SCRIPT-ADM
- IP-CIDR,10.0.0.0/8,ChuKK-SCRIPT-ADM
- IP-CIDR,172.16.0.0/12,ChuKK-SCRIPT-ADM
- IP-CIDR,127.0.0.0/8,ChuKK-SCRIPT-ADM
- IP-CIDR,100.64.0.0/10,ChuKK-SCRIPT-ADM

# DNSPod Public DNS+
- IP-CIDR,119.28.28.28/32,ChuKK-SCRIPT-ADM,no-resolve
# GeoIP China
- GEOIP,CN,ChuKK-SCRIPT-ADM

- MATCH,ChuKK-SCRIPT-ADM

proxies:' >> /root/.config/clash/config.yaml 
[[ $mode = 2 ]] && echo -e '
proxies:' >> /root/.config/clash/config.yaml 
}

conFIN() {
confRULE
[[ ! -z ${proTRO} ]] && echo -e "${proTRO}" >> /root/.config/clash/config.yaml
[[ ! -z ${proV2R} ]] && echo -e "${proV2R}" >> /root/.config/clash/config.yaml
[[ ! -z ${proXR} ]] && echo -e "${proXR}" >> /root/.config/clash/config.yaml

#echo ''

echo "#POWER BY @ChuKK-SCRIPT" >> /root/.config/clash/config.yaml
}

enon(){
		clear
		msg -bar3
		blanco " Se ha agregado un autoejecutor en el Sector de Inicios Rapidos"
		msg -bar3
		blanco "	  Para Acceder al menu Rapido \n	     Utilize * clash.sh * !!!"
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi deseas desabilitar esta opcion, apagala"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
}
enoff(){
rm -f /bin/clash.sh
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSe ha Desabilitado el menu Rapido de clash.sh"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
}

enttrada () {
echo 'source <(curl -sSL https://gitlab.com/patomods/drowkid01/-/raw/main/exec/Recursos/ClashForAndroidGLOBAL.sh)' > /bin/clash.sh && chmod +x /bin/clash.sh
}

blanco(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;37m$1\033[0m"
	} || {
		echo -ne " \033[1;37m$1:\033[0m "
	}
}
title(){
	msg -bar3
	blanco "$1"
	msg -bar3
}
col(){
	nom=$(printf '%-55s' "\033[0;92m${1} \033[0;31m>> \033[1;37m${2}")
	echo -e "	$nom\033[0;31m${3}   \033[0;92m${4}\033[0m"
}
col2(){
	echo -e " \033[1;91m$1\033[0m \033[1;37m$2\033[0m"
}
vacio(){
blanco "\n no se puede ingresar campos vacios..."
}
cancelar(){
echo -e "\n \033[3;49;31minstalacion cancelada...\033[0m"
}
continuar(){
echo -e " \033[3;49;32mEnter para continuar...\033[0m"
}
userDat(){
	blanco "	NÂ°    Usuarios 		  fech exp   dias"
	msg -bar3
}
view_usert(){
configt="/usr/local/etc/trojan/config.json"
tempt="/etc/trojan/temp.json"
trojdirt="/etc/trojan" 
user_conf="/etc/trojan/user"
backdirt="/etc/trojan/back" 
tmpdirt="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
	nick="$(cat $configt | grep ',"')"
	users="$(cat $configt | jq -r .password[])"
		title "	ESCOJE USUARIO TROJAN"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = chumoghscript ]] && {
				Usr="Admin"
				DateExp=" Ilimitado"
			} || {
			Usr="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f1)"
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}
			col "$n)" "${Usr}" "$DateExp" "$exp"
			let n++
		done
		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "SELECCIONA USUARIO" 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && tropass="user_null" && break
		n=1
		unset i
		for i in $users
		do
		[[ $n = $opcion ]] && tropass=$i
			let n++
		done
		let opcion--
		addip=$(wget -qO- ifconfig.me)
		host=$(cat $configt | jq -r .ssl.sni)
		trojanport=$(cat $configt | jq -r .local_port)
		UUID=$(cat $configt | jq -r .password[$opcion])
		Usr="$(cat ${user_conf}|grep -w "${UUID}"|cut -d'|' -f1)"
		echo "USER ${Usr} : $UUID " 
		break
	done
}

view_user(){
config="/etc/v2ray/config.json"
temp="/etc/v2ray/temp.json"
v2rdir="/etc/v2r" && [[ ! -d $v2rdir ]] && mkdir $v2rdir
user_conf="/etc/v2r/user" && [[ ! -e $user_conf ]] && touch $user_conf
backdir="/etc/v2r/back" && [[ ! -d ${backdir} ]] && mkdir ${backdir}
tmpdir="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

		title "	VER USUARIO V2RAY REGISTRADO"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = null ]] && {
				i="Admin"
				DateExp=" Ilimitado"
			} || {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}

			col "$n)" "$i" "$DateExp" "$exp"
			let n++
		done

		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "Escoje Tu Usuario : " 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && break
		let opcion--
		ps=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		uid=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiid=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host=''
		net=$(jq '.inbounds[].streamSettings.network' $config)
		parche=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $path = null ]] && parche='' 
		v2port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addip=$(wget -qO- ifconfig.me)
		echo "Usuario $ps Seleccionado" 
		break
	done
}

_view_userXR(){
config="/etc/xray/config.json"
temp="/etc/xray/temp.json"
v2rdir="/etc/xr" && [[ ! -d $v2rdir ]] && mkdir $v2rdir
user_conf="/etc/xr/user" && [[ ! -e $user_conf ]] && touch $user_conf
backdir="/etc/xr/back" && [[ ! -d ${backdir} ]] && mkdir ${backdir}
tmpdir="$backdir/tmp"
	unset seg
	seg=$(date +%s)
	while :
	do
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

		title "	VER USUARIO XRAY REGISTRADO"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = null ]] && {
				i="Admin"
				DateExp=" Ilimitado"
			} || {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}

			col "$n)" "$i" "$DateExp" "$exp"
			let n++
		done

		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "Escoje Tu Usuario : " 0
		read opcion
		[[ -z $opcion ]] && vacio && sleep 0.3s && continue
		[[ $opcion = 0 ]] && break
		let opcion--
		psX=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $psX = null ]] && ps="default"
		uidX=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aluuiidX=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		addX=$(jq '.inbounds[].domain' $config) && [[ $addX = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		hostX=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $hostX = null ]] && hostX=''
		netX=$(jq '.inbounds[].streamSettings.network' $config)
		parcheX=$(jq -r .inbounds[].streamSettings.wsSettings.path $config) && [[ $pathX = null ]] && parcheX='' 
		v2portX=$(jq '.inbounds[].port' $config)
		tlsX=$(jq '.inbounds[].streamSettings.security' $config)
		[[ $netX = '"grpc"' ]] && pathX=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || pathX=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		addipX=$(wget -qO- ifconfig.me)
		echo "Usuario XRAY SERA  $psX Seleccionado" 
		break
	done
}

[[ ! -d /root/.config/clash ]] && fun_insta || fun_ip
clear
[[ -e /root/name ]] && figlet -p -f slant < /root/name || echo -e "\033[7;49;35m    =====>>â–ºâ–º ðŸ² New ChuKK-SCRIPTðŸ’¥VPS ðŸ² â—„â—„<<=====      \033[0m"
fileon=$(ls -la /var/www/html | grep "yaml" | wc -l)
filelo=$(ls -la /root/.config/clash | grep "yaml" | wc -l)
cd
msg -bar3
echo -e "\033[1;37m ✬  Linux Dist: $(less /etc/issue.net)\033[0m"
msg -bar3
echo -e "\033[1;37m ✬ Ficheros Online:	$fileon  ✬ Ficheros Locales: $filelo\033[0m"
msg -bar3
echo -e "\033[1;37m - Menu Iterativo Clash for Android - ChuKK-SCRIPT \033[0m"
msg -bar3
echo -e "\033[1;37mSeleccione :    Para Salir Ctrl + C o 0 Para Regresar\033[1;33m"
unset yesno
echo -e " DESEAS CONTINUAR CON LA CARGA DE CONFIG CLASH?"
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p "[S/N]: " yesno
tput cuu1 && tput dl1
done
if [[ ${yesno} = @(s|S|y|Y) ]]; then
unset yesno numwt
#[[ -e /root/name ]] && figlet -p -f slant < /root/name || echo -e "\033[7;49;35m    =====>>â–ºâ–º ðŸ² New ChuKK-SCRIPTðŸ’¥VPS ðŸ² â—„â—„<<=====      \033[0m"
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33m ✬ Ingresa tu Whatsapp junto a tu codigo de Pais"
read -p " Ejemplo: +593987072611 : " numwt
if [[ -z $numwt ]]; then
numwt='+593987072611'
fi
echo -e "[\033[1;31m-\033[1;33m]\033[1;31m \033[1;33m"
echo -e "\033[1;33m ✬ Ingresa Clase de Servidor ( Gratis - PREMIUM )"
read -p " Ejemplo: PREMIUM : " srvip
if [[ -z $srvip ]]; then
srvip="NewADM"
fi
	while :
	do
	[[ -z ${opcion} ]] || break
		clear
		echo -e " ESCOJE TU METODO DE SELECCION "
		echo -e "  "
		echo -e " SINO CONOCES DE ESTO, ESCOJE 2 "
		echo -e "  "
		msg -bar
		echo -e "1 - SELECTOR RULES"
		echo -e "2 - SELECTOR GLOBAL"
		msg -bar
		echo -e " 0) CANCELAR"
		msg -bar
		read -p " ESCOJE : " opcion
		case $opcion in
			1)configINIT_rule "$opcion"
			break;;
			2)configINIT_global "$opcion"
			break;;
			0) break;;
			*) echo -e "\n selecione una opcion del 0 al 2" && sleep 0.3s;;
		esac
	done
INITClash
fi
