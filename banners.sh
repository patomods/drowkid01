
function banner_dropbear(){
clear;msg -bar;echo -e "
\e[38;5;190m╺┳┓┏━┓┏━┓┏━┓┏┓ ┏━╸┏━┓┏━┓
\e[38;5;192m ┃┃┣┳┛┃ ┃┣━┛┣┻┓┣╸ ┣━┫┣┳┛
\e[38;5;194m╺┻┛╹┗╸┗━┛╹  ┗━┛┗━╸╹ ╹╹┗╸
"
msg -bar
}
function banner_ssl(){
clear;msg -bar;echo -e "
\e[38;5;190m┏━┓┏━┓╻  
\e[38;5;192m┗━┓┗━┓┃  
\e[38;5;194m┗━┛┗━┛┗━╸
"
msg -bar
}
function banner_sockspy(){
clear;msg -bar;echo -e "
\e[38;5;190m┏━┓┏━┓┏━╸╻┏ ┏━┓┏━┓╻ ╻
\e[38;5;192m┗━┓┃ ┃┃  ┣┻┓┗━┓┣━┛┗┳┛
\e[38;5;194m┗━┛┗━┛┗━╸╹ ╹┗━┛╹   ╹ 
"
msg -bar
}
function banner_v2ray(){
clear;msg -bar;echo -e "
\e[38;5;190m╻ ╻┏━┓┏━┓┏━┓╻ ╻
\e[38;5;192m┃┏┛┏━┛┣┳┛┣━┫┗┳┛
\e[38;5;194m┗┛ ┗━╸╹┗╸╹ ╹ ╹ 
"
msg -bar
}
function banner_slowdns(){
clear;msg -bar;echo -e "
\e[38;5;190m┏━┓╻  ┏━┓╻ ╻╺┳┓┏┓╻┏━┓
\e[38;5;192m┗━┓┃  ┃ ┃┃╻┃ ┃┃┃┗┫┗━┓
\e[38;5;194m┗━┛┗━╸┗━┛┗┻┛╺┻┛╹ ╹┗━┛
"
msg -bar
}
function banner_xray(){
clear;msg -bar;echo -e "
\e[38;5;190m╻ ╻┏━┓┏━┓╻ ╻
\e[38;5;192m┏╋┛┣┳┛┣━┫┗┳┛
\e[38;5;194m╹ ╹╹┗╸╹ ╹ ╹ 
"
msg -bar
}
function banner_udps(){
clear;msg -bar;echo -e "
\e[38;5;190m╻ ╻╺┳┓┏━┓┏━┓
\e[38;5;192m┃ ┃ ┃┃┣━┛┗━┓
\e[38;5;194m┗━┛╺┻┛╹  ┗━┛
"
msg -bar
}
function banner_ss-ssr(){
clear;msg -bar;echo -e "
\e[38;5;190m┏━┓┏━┓   ┏━┓┏━┓┏━┓
\e[38;5;192m┗━┓┗━┓╺━╸┗━┓┗━┓┣┳┛
\e[38;5;194m┗━┛┗━┛   ┗━┛┗━┛╹┗╸
"
msg -bar
}
function banner_badvpn(){
clear;msg -bar;echo -e "
\e[38;5;190m┏┓ ┏━┓╺┳┓╻ ╻┏━┓┏┓╻
\e[38;5;192m┣┻┓┣━┫ ┃┃┃┏┛┣━┛┃┗┫
\e[38;5;194m┗━┛╹ ╹╺┻┛┗┛ ╹  ╹ ╹
"
msg -bar
}
function banner_openvpn(){
clear;msg -bar;echo -e "
\e[38;5;190m┏━┓┏━┓┏━╸┏┓╻╻ ╻┏━┓┏┓╻
\e[38;5;192m┃ ┃┣━┛┣╸ ┃┗┫┃┏┛┣━┛┃┗┫
\e[38;5;194m┗━┛╹  ┗━╸╹ ╹┗┛ ╹  ╹ ╹
"
msg -bar
}
case $1 in
 --dropbear) banner_dropbear;;
 --ssl) banner_ssl;;
 --sockspy) banner_sockspy;;
 --v2ray) banner_v2ray;;
 --slowdns) banner_slowdns;;
 --xray) banner_xray;;
 --udps) banner_udps;;
 --ss-ssr) banner_ss-ssr;;
 --badvpn) banner_badvpn;;
 --openvpn) banner_openvpn;;
esac
