#!/bin/bash
source <(wget -qO- https://gitlab.com/patomods/drowkid01/-/raw/main/colores) &> /dev/null

declare -A sdir=( [0]="." [log]=".log" )

function fun_ip(){
	ip1=$(wget -qO- ifconfig.me);ip2=$(wget -qO- ipv4.icanhazip.com)
		[[ $ip1 != $ip2 ]] && ip='' || ip=$(echo $ip2|grep $ip1)
}

init(){
msg -tit;menu_info;msg -bar
title=`echo -e "\033[1;96m${ress:="@drowkid01"}"`
printf "%*s\n" $(( ( $(echo -e "$title" | wc -c ) + 55 ) / 2 )) "$title"
msg -bar
}

menu(){
init
menu_func "ADMINISTRAR CUENTAS | SSH/SSL/DROPBEAR" "ADMINISTRAR CUENTAS | SS/SSR" "ADMINISTRAR CUENTAS | XRAY/V2RAY" "\e[1;31m\033[47mPROTOCOLOS\e[0m  \e[93m||  \e[1;37m\e[41mHERRAMIENTAS\e[0m" "MONITOR DE PROTOCOLOS" "-bar AUTO-INICIO SCRIPT" "\e[1;32mACTUALIZACIÓN" "-vm DESINSTALAR" "-bar CRÉDITOS"
}

menu_inst(){
	inst(){
	source banners.sh "$1"
	   [[ ! -e "${sdir[log]}/.$(echo $1|tr -d "-").log" ]] && {
		menu_func "INSTALAR $(echo $1|tr -d "-") [#default]" "INSTALAR $(echo $1|tr -d "-") [#chukk]" "INSTALAR $(echo $1|tr -d "-") [#@rufu99]" "-bar INSTALAR $(echo $1|tr -d "-") [#experimental]"
	   } || { menu_func "EDITAR PUERTO[S] $(echo $1|tr -d "-")" "-vm DESINSTALAR $(echo $1|tr -d "-")" "-bar FIX BASEINS_$(echo $1|tr -d "-")" ; }
		case `selection_fun 5` in
		 1);;
		esac
	}

tpu "11"
n=1
for protocolo in `echo "badvpn socks v2ray ssl dropbear squid openvpn slowdns bot-tg"`; do
[[ ! -e ${sdir[log]}/.$protocolo.log ]] && declare -A inst[$protocolo]+="\e[1;31m$protocolo" || declare -A inst[$protocolo]+="\e[1;32m$protocolo"
	axp=$(jq '{protocolo1: "archivo online", protocolo2: "firewall",protocolo3: "fail2ban protection", protocolo4: "detalles del sistema",protocolo5: "tcp [bbr|bbr-plus]",protocolo6: "dns-netflix", protocolo7: "auto-mantenimiento", protocolo8: "networktools",protocolo9: "speedtest"  }' -n |jq -r .protocolo$n)
	echo -ne " \e[1;30m[\e[91m$n\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97m${inst[$protocolo]}     	";echo -e " \e[1;30m[\e[91m1$n\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97m$axp"
	n=$(( $n + 1))
done
echo -ne "\e[1;30m[\e[91m10\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97mmonitorapp  ${inst[monitorapp]}";echo -e "    \e[1;30m[\e[91m20\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;7;33mherramientas básicas \e[0m"
msg -bar
echo -ne "\e[1;30m[\e[91m21\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97mws-pro 	${inst[monitorapp]}";echo -e "\e[1;30m[\e[91m23\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97mherramientas básicas \e[0m"
echo -ne "\e[1;30m[\e[91m22\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97mudps	  	${inst[monitorapp]}";echo -e "\e[1;30m[\e[91m24\e[1;30m] \e[1;31m⟩\e[1;30m⟩\e[1;31m⟩ \e[1;97mherramientas básicas \e[0m"
msg -bar
ux=`selection_fun 29`
	case `selection_fun 33` in
	1)inst "--badvpn" ;;
	2)inst "--sockspy" ;;
	3)inst "--v2ray" ;;
	4)inst "--ssl" ;;
	5)inst "--dropbear" ;;
	6)inst "--squid" ;;
	7)inst "--openvpn" ;;
	8)inst "--slowdns" ;;
	9)inst "--badvpn" ;;
	10)inst "--badvpn" ;;
	11)inst "--badvpn" ;;
	esac
}

fun_eth(){
eth=$(ifconfig | grep -v inet6 | grep -v lo | grep -v 127.0.0.1 | grep "encap:Ethernet" | awk '{print $1}')
    [[ $eth != "" ]] && {
    msg -bar;msg -ama " $(fun_trans "Aplicar el sistema para mejorar los paquetes SSH?")";msg -ama " $(fun_trans "Opciones para usuarios avanzados")";msg -bar
    read -p " [S/N]: " -e -i n sshsn
           [[ "$sshsn" = @(s|S|y|Y) ]] && {
           echo -e "${cor[1]} $(fun_trans "Correccion de problemas de paquetes en SSH ...")"
           echo -e " $(fun_trans "¿Cual es la tasa RX?")"
           echo -ne "[ 1 - 999999999 ]: "; read rx
           [[ "$rx" = "" ]] && rx="999999999"
           echo -e " $(fun_trans "¿Cual es la tasa TX?")"
           echo -ne "[ 1 - 999999999 ]: "; read tx
           [[ "$tx" = "" ]] && tx="999999999"
           apt-get install ethtool -y > /dev/null 2>&1
           ethtool -G $eth rx $rx tx $tx > /dev/null 2>&1
           }
     msg -bar
     }
}

menu_info(){
fun_ip &>/dev/null
	if [[ "$(grep -c "Ubuntu" /etc/issue.net)" = "1" ]]; then
		system=$(cut -d' ' -f1 /etc/issue.net);system+=$(echo ' ');system+=$(cut -d' ' -f2 /etc/issue.net |awk -F "." '{print $1}')
	elif [[ "$(grep -c "Debian" /etc/issue.net)" = "1" ]]; then
		system=$(cut -d' ' -f1 /etc/issue.net);system+=$(echo ' ');system+=$(cut -d' ' -f3 /etc/issue.net)
	else
		system=$(cut -d' ' -f1 /etc/issue.net)
	fi
	_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")

	_ram=$(printf ' %-8s' "$(free -h | grep -i mem | awk {'print $2'})");_ram2=$(printf ' %-8s' "$(free -h | grep -i mem | awk {'print $4'})")
	_system=$(printf '%-9s' "$system");_core=$(printf '%-8s' "$(grep -c cpu[0-9] /proc/stat)")
	_usop=$(top -bn1 | sed -rn '3s/[^0-9]* ([0-9\.]+) .*/\1/p;4s/.*, ([0-9]+) .*/\1/p' | tr '\n' ' ')

	modelo1=$(printf '%-11s' "$(lscpu | grep Arch | sed 's/\s\+/,/g' | cut -d , -f2)");mb=$(printf '%-8s' "$(free -h | grep Mem | sed 's/\s\+/,/g' | cut -d , -f6)")
	_hora=$(printf '%(%H:%M:%S)T');_hoje=$(date +'%d/%m/%Y')

echo -e "\033[1;37m OS \033[1;31m: \033[1;32m$_system \033[1;37mHORA\033[1;31m: \033[1;32m$_hora  \033[1;37mIP\033[1;31m:\033[1;32m $ip"
echo -e "\033[1;37m RAM\e[31m: \033[1;32m$_ram \033[1;37mUSADO\033[1;31m: \033[1;32m$mb\033[1;37m LIBRE\033[1;31m: \033[1;32m$_ram2"

}

menu
case `selection_fun 11` in
1) usercodes --ssh;;
2) usercodes --ss;;
3) usercodes --vmess;;
4) menu_inst;;
esac
